import QtQuick
import Felgo

Item {
    id: bone

    width: sprite.width
    height: sprite.height

    MultiResolutionImage {
        id: sprite
        source: Qt.resolvedUrl("../../assets/bone.png")
    }
}
