import QtQuick
import Felgo

Item {
    id: button

    signal clicked
    signal pressed
    signal released

    width: sprite.width
    height: sprite.height

    MultiResolutionImage {
        id: sprite
        source: Qt.resolvedUrl("../../assets/playAgain.png")
    }

    MouseArea {
        id: mouseArea
        enabled: button.enabled
        anchors.fill: button
        hoverEnabled: true

        onClicked: button.clicked()
    }

    onClicked: {
    }
}
