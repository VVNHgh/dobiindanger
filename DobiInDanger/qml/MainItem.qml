import QtQuick
import Felgo
import "scenes"
import "common"

Item {
    id: mainItem
    property alias entityManager: entityManager

    GameScene {
        id: gameScene
    }

    MenuScene {
        id: menuScene
        onGamePressed: {
            mainItem.state = "game"
        }

        Connections {
            // NativeUtils should only be connected, when this is the active scene
            target: gameWindow.activeScene === menuScene ? NativeUtils : null
            onMessageBoxFinished: accepted => {
                                      if(accepted) {
                                          Qt.quit()
                                      }
                                  }
        }
    }

    EntityManager {
        id: entityManager
        // entities shall only be created in the gameScene
        entityContainer: gameScene.entityContainer
    }

    state: "menu"

    states: [
        State {
            name: "menu"
            PropertyChanges {target: menuScene; opacity: 1}
            PropertyChanges {target: gameScene; opacity: 0}
            PropertyChanges {target: gameWindow; activeScene: menuScene}

        },
        State {
            name: "game"
            PropertyChanges {target: gameScene; opacity: 1}
            PropertyChanges {target: menuScene; opacity: 0}
            PropertyChanges {target: gameWindow; activeScene: gameScene}
            StateChangeScript {
                script: {
                    gameScene.enterScene()
                }
            }
        }
    ]

}
