import QtQuick
import Felgo

EntityBase {
    id: player
    entityId: "player"
    entityType: "player"

    property var board:
    [
        [ 7,  2, 2, 2,  2, 2,  2, 2, 2, 2, 2,  2,  2, 2, 2,  2, 2, 2, 2,  2, 8  ],
        [ 5,  0, 0, 0,  0, 0,  0, 0, 0, 0, 0,  0,  0, 0, 0,  0, 0, 0, 0,  0, 5  ],
        [ 5,  0, 1, 2,  2, 2,  2, 2, 2, 2, 2,  2,  2, 2, 2,  2, 2, 2, 3,  0, 5  ],
        [ 5,  0, 0, 0,  0, 0,  0, 0, 0, 0, 0,  0,  0, 0, 0,  0, 0, 0, 0,  0, 5  ],
        [ 5,  0, 1, 2,  2, 2,  2, 8, 0, 7, 0,  8,  0, 7, 2,  2, 2, 2, 3,  0, 5  ],
        [ 5,  0, 0, 0,  0, 0,  0, 5, 0, 5, 0,  5,  0, 5, 0,  0, 0, 0, 0,  0, 5  ],
        [ 11, 2, 2, 2,  2, 8,  0, 5, 0, 5, 0,  5,  0, 5, 0,  7, 2, 2, 2,  2, 12 ],
        [ 5,  0, 0, 0,  0, 5,  0, 5, 0, 5, 0,  5,  0, 5, 0,  5, 0, 0, 0,  0, 5  ],
        [ 5,  0, 7, 8,  0, 5,  0, 5, 0, 5, 0,  5,  0, 5, 0,  5, 0, 7, 8,  0, 5  ],
        [ 5,  0, 9, 10, 0, 5,  0, 5, 0, 9, 2,  10, 0, 5, 0,  5, 0, 7, 8,  0, 5  ],
        [ 5,  0, 0, 0,  0, 5,  0, 5, 0, 0, 0,  0,  0, 5, 0,  5, 0, 0, 0,  0, 5  ],
        [ 11, 2, 2, 3,  0, 6,  0, 6, 0, 1, 14, 3,  0, 6, 0,  6, 0, 1, 2,  2, 12 ],
        [ 5,  0, 0, 0,  0, 0,  0, 0, 0, 0, 5,  0,  0, 0, 0,  0, 0, 0, 0,  0, 5  ],
        [ 5,  0, 7, 8,  0, 4,  0, 1, 2, 2, 13, 2,  2, 3, 0,  4, 0, 7, 8,  0, 5  ],
        [ 5,  0, 9, 10, 0, 5,  0, 0, 0, 0, 0,  0,  0, 0, 0,  5, 0, 9, 10, 0, 5  ],
        [ 5,  0, 0, 0,  0, 5,  0, 0, 0, 0, 0,  0,  0, 0, 0,  5, 0, 0, 0,  0, 5  ],
        [ 9,  2, 2, 2,  2, 13, 2, 2, 2, 2, 2,  2,  2, 2, 2, 13, 2, 2, 2,  2, 10 ]
    ]

    property int resetX: 0
    property int resetY: 0
    property int boxWidth: scene.gameWindowAnchorItem.width
    property int boxHeight: scene.gameWindowAnchorItem.height
    property string walkingImage: ""

    property int numberOfBoxX: 21
    property int numberOfBoxY: 17

    property int sizeOfOneBoxX: boxWidth/numberOfBoxX

    property int movingPixels: 50
    property int checkNext: 15
    // R, L, U, D
    property int movingDirection: 0

    property bool facingLeft: false

    signal stopHorizontalAnim()
    signal stopVerticalAnim()
    signal stopWalking()
    signal startWalking()

    onStopHorizontalAnim: {
        walkingSequence.running = false;
        animationX.stop();
    }

    onStopVerticalAnim: {
        walkingSequence.running = false;
        animationY.stop();
    }

    onStopWalking: {
        walkingSequence.running = false;
    }

    onStartWalking: {
        walkingSequence.running = false;
    }

    GameSpriteSequence {
        id: walkingSequence
        anchors.centerIn: parent
        mirrorX: player.facingLeft

        GameSprite {
           frameCount: 2
           frameRate: 10
           frameWidth: 44
           frameHeight: 36
           source: walkingImage
        }
    }

    MovementAnimation {
        id: animationX
        target: parent
        property: "x"
        velocity: 0
        running: false
        minPropertyValue: sizeOfOneBoxX/2
        maxPropertyValue: boxWidth - player.width
        onLimitReached: {
            walkingSequence.running = false;
        }
    }

    MovementAnimation {
        id: animationY
        target: parent
        property: "y"
        velocity: 0
        running: false
        minPropertyValue: sizeOfOneBoxX/2
        maxPropertyValue: boxHeight - player.width
        onLimitReached: {
            walkingSequence.running = false;
        }
    }

    function changeDirectionToHorizontal() {
        animationY.stop();
        animationX.start();
        walkingSequence.running = true;
    }

    function changeDirectionToVertical() {
        animationX.stop();
        animationY.start();
        walkingSequence.running = true;
    }

    state: "right"

    states: [
        State {
            name: "right"
            PropertyChanges {target: animationX; velocity: player.movingPixels}
            StateChangeScript {
                script: {
                    movingDirection = 0;
                    changeDirectionToHorizontal();

                    facingLeft = false;
                }
            }
        },
        State {
            name: "left"
            PropertyChanges {target: animationX; velocity: -player.movingPixels}
            StateChangeScript {
                script: {
                    movingDirection = 1;
                    console.log("GOAT CHECK ", boxWidth, board);
                    changeDirectionToHorizontal();
                    facingLeft = true;
                }
            }
        },
        State {
            name: "up"
            PropertyChanges {target: animationY; velocity: -player.movingPixels}
            StateChangeScript {
                script: {
                    movingDirection = 2;
                    changeDirectionToVertical();
                }
            }
        },
        State {
            name: "down"
            PropertyChanges {target: animationY; velocity: player.movingPixels}
            StateChangeScript {
                script: {
                    movingDirection = 3;
                    changeDirectionToVertical();
                }
            }
        }
    ]

}
