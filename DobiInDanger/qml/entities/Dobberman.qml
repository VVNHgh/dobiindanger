import QtQuick
import Felgo

EntityBase {
    id: dobberman
    entityType: "dobberman"
    width: 36
    height: 36

    property var board:
    [
        [ 7,  2, 2, 2,  2, 2,  2, 2, 2, 2, 2,  2,  2, 2, 2,  2, 2, 2, 2,  2, 8  ],
        [ 5,  0, 0, 0,  0, 0,  0, 0, 0, 0, 0,  0,  0, 0, 0,  0, 0, 0, 0,  0, 5  ],
        [ 5,  0, 1, 2,  2, 2,  2, 2, 2, 2, 2,  2,  2, 2, 2,  2, 2, 2, 3,  0, 5  ],
        [ 5,  0, 0, 0,  0, 0,  0, 0, 0, 0, 0,  0,  0, 0, 0,  0, 0, 0, 0,  0, 5  ],
        [ 5,  0, 1, 2,  2, 2,  2, 8, 0, 7, 0,  8,  0, 7, 2,  2, 2, 2, 3,  0, 5  ],
        [ 5,  0, 0, 0,  0, 0,  0, 5, 0, 5, 0,  5,  0, 5, 0,  0, 0, 0, 0,  0, 5  ],
        [ 11, 2, 2, 2,  2, 8,  0, 5, 0, 5, 0,  5,  0, 5, 0,  7, 2, 2, 2,  2, 12 ],
        [ 5,  0, 0, 0,  0, 5,  0, 5, 0, 5, 0,  5,  0, 5, 0,  5, 0, 0, 0,  0, 5  ],
        [ 5,  0, 7, 8,  0, 5,  0, 5, 0, 5, 0,  5,  0, 5, 0,  5, 0, 7, 8,  0, 5  ],
        [ 5,  0, 9, 10, 0, 5,  0, 5, 0, 9, 2,  10, 0, 5, 0,  5, 0, 7, 8,  0, 5  ],
        [ 5,  0, 0, 0,  0, 5,  0, 5, 0, 0, 0,  0,  0, 5, 0,  5, 0, 0, 0,  0, 5  ],
        [ 11, 2, 2, 3,  0, 6,  0, 6, 0, 1, 14, 3,  0, 6, 0,  6, 0, 1, 2,  2, 12 ],
        [ 5,  0, 0, 0,  0, 0,  0, 0, 0, 0, 5,  0,  0, 0, 0,  0, 0, 0, 0,  0, 5  ],
        [ 5,  0, 7, 8,  0, 4,  0, 1, 2, 2, 13, 2,  2, 3, 0,  4, 0, 7, 8,  0, 5  ],
        [ 5,  0, 9, 10, 0, 5,  0, 0, 0, 0, 0,  0,  0, 0, 0,  5, 0, 9, 10, 0, 5  ],
        [ 5,  0, 0, 0,  0, 5,  0, 0, 0, 0, 0,  0,  0, 0, 0,  5, 0, 0, 0,  0, 5  ],
        [ 9,  2, 2, 2,  2, 13, 2, 2, 2, 2, 2,  2,  2, 2, 2, 13, 2, 2, 2,  2, 10 ]
    ]

    property int resetX: 0
    property int resetY: 0

    property int boxWidth: scene.gameWindowAnchorItem.width
    property int boxHeight: scene.gameWindowAnchorItem.height
    property int numberOfBoxX: 21
    property int numberOfBoxY: 17
    property int sizeOfOneBoxX: boxWidth/numberOfBoxX

    property int movingPixels: 60
    property int checkNext: 15
    // R, L, U, D
    property int movingDirection: 0

    property bool facingLeft: false
    property bool isPredator: false

    signal gameOver()
    signal gameWon()

    Component.onCompleted: reset()

    onGameOver: {
        checkPozTimer.stop();
        walkingSequence.running = false;
        animationX.stop();
        animationY.stop();
    }

    onGameWon: {
        checkPozTimer.stop();
        walkingSequence.running = false;
        animationX.stop();
        animationY.stop();
    }

    GameSpriteSequence {
        id: walkingSequence
        anchors.centerIn: parent
        mirrorX: dobberman.facingLeft

        GameSprite {
           id: walkingSequenceImage
           frameCount: 2
           frameRate: 10
           frameWidth: 44
           frameHeight: 36
           source: "../../../assets/dobiWalkingSequence.png"
        }
    }

    MovementAnimation {
        id: animationX
        target: parent
        property: "x"
        velocity: 0
        running: false
        minPropertyValue: sizeOfOneBoxX/2
        maxPropertyValue: boxWidth - dobberman.width
        onLimitReached: {
            walkingSequence.running = false;
        }
    }

    MovementAnimation {
        id: animationY
        target: parent
        property: "y"
        velocity: 0
        running: false
        minPropertyValue: sizeOfOneBoxX/2
        maxPropertyValue: boxHeight - dobberman.width
        onLimitReached: {
            walkingSequence.running = false;
        }
    }

    Timer {
        id: checkPozTimer
        interval: 300
        repeat: true
        onTriggered: {
            checkPosition(movingDirection);
        }
    }

    function reset() {
        opacity = 1;
        checkPozTimer.start();
        dobberman.x = resetX;
        dobberman.y = resetY;
        walkingSequence.running = true;
        animationX.start();
    }

    function stopGame() {
        checkPozTimer.stop();
        opacity = 0;
        walkingSequence.running = false;
        animationX.stop();
        animationY.stop();
    }

    function makePredator() {
        isPredator = true;
        movingPixels += 20
        walkingSequenceImage.source = "../../../assets/predatorWalkingSequence.png"
    }

    function predatorNoMore() {
        isPredator = false;
        movingPixels -= 20;
        walkingSequenceImage.source = "../../../assets/dobiWalkingSequence.png"
    }

    function changeDirectionToHorizontal() {
        animationY.stop();
        animationX.start();
        walkingSequence.running = true;
    }

    function changeDirectionToVertical() {
        animationX.stop();
        animationY.start();
        walkingSequence.running = true;
    }

    function goRight() {
        if(checkPosition(0)) {
            dobberman.state = "right";
        }
    }

    function goLeft() {
        if(checkPosition(1)) {
            dobberman.state = "left";
        }
    }

    function goUp() {
        if(checkPosition(2)) {
            dobberman.state = "up";
        }
    }

    function goDown() {
        if(checkPosition(3)) {
            dobberman.state = "down";
        }
    }

    function checkPosition(movingDir) {

        var nextPozI = 0;
        var nextPozJ = 0;

        // R, L, U, D
        switch(movingDir) {
            case 0:
                nextPozJ = Math.min(numberOfBoxX-1, Math.ceil((dobberman.x + dobberman.width/2 + checkNext)/sizeOfOneBoxX)-1);
                nextPozI = Math.ceil((dobberman.y + dobberman.height/2)/sizeOfOneBoxX)-1;
                if(board[nextPozI][nextPozJ] !== 0) {
                    animationX.stop();
                    return false;
                }

                break;
            case 1:
                nextPozJ = Math.max(0, Math.ceil((dobberman.x + dobberman.width/2 - checkNext)/sizeOfOneBoxX)-1);
                nextPozI = Math.ceil((dobberman.y + dobberman.height/2)/sizeOfOneBoxX)-1;
                if(board[nextPozI][nextPozJ] !== 0) {
                    animationX.stop();
                    return false;
                }
                break;
            case 2:
                nextPozJ = Math.ceil((dobberman.x + dobberman.width/2)/sizeOfOneBoxX)-1;
                nextPozI = Math.max(0, Math.ceil((dobberman.y + dobberman.height/2 - checkNext)/sizeOfOneBoxX)-1);
                if(board[nextPozI][nextPozJ] !== 0) {
                    animationY.stop();
                    return false;
                }
                break;
            case 3:
                nextPozJ = Math.ceil((dobberman.x + dobberman.width/2)/sizeOfOneBoxX)-1;
                nextPozI = Math.min(numberOfBoxY-1, Math.ceil((dobberman.y + dobberman.height/2 + checkNext)/sizeOfOneBoxX)-1);
                if(board[nextPozI][nextPozJ] !== 0) {
                    animationY.stop();
                    return false;
                }
                break;
        }
        return true;
    }

    state: "right"

    states: [
        State {
            name: "right"
            PropertyChanges {target: animationX; velocity: dobberman.movingPixels}
            StateChangeScript {
                script: {
                    movingDirection = 0;
                    changeDirectionToHorizontal();

                    facingLeft = false;
                }
            }
        },
        State {
            name: "left"
            PropertyChanges {target: animationX; velocity: -dobberman.movingPixels}
            StateChangeScript {
                script: {
                    movingDirection = 1;
                    changeDirectionToHorizontal();
                    facingLeft = true;
                }
            }
        },
        State {
            name: "up"
            PropertyChanges {target: animationY; velocity: -dobberman.movingPixels}
            StateChangeScript {
                script: {
                    movingDirection = 2;
                    changeDirectionToVertical();
                }
            }
        },
        State {
            name: "down"
            PropertyChanges {target: animationY; velocity: dobberman.movingPixels}
            StateChangeScript {
                script: {
                    movingDirection = 3;
                    changeDirectionToVertical();
                }
            }
        }
    ]
}
