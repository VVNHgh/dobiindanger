import QtQuick
import Felgo

EntityBase {
    id: vacuum
    entityType: "vacuum"
    width: 50
    height: 23

    property var board:
    [
        [ 7,  2, 2, 2,  2, 2,  2, 2, 2, 2, 2,  2,  2, 2, 2,  2, 2, 2, 2,  2, 8  ],
        [ 5,  0, 0, 0,  0, 0,  0, 0, 0, 0, 0,  0,  0, 0, 0,  0, 0, 0, 0,  0, 5  ],
        [ 5,  0, 1, 2,  2, 2,  2, 2, 2, 2, 2,  2,  2, 2, 2,  2, 2, 2, 3,  0, 5  ],
        [ 5,  0, 0, 0,  0, 0,  0, 0, 0, 0, 0,  0,  0, 0, 0,  0, 0, 0, 0,  0, 5  ],
        [ 5,  0, 1, 2,  2, 2,  2, 8, 0, 7, 0,  8,  0, 7, 2,  2, 2, 2, 3,  0, 5  ],
        [ 5,  0, 0, 0,  0, 0,  0, 5, 0, 5, 0,  5,  0, 5, 0,  0, 0, 0, 0,  0, 5  ],
        [ 11, 2, 2, 2,  2, 8,  0, 5, 0, 5, 0,  5,  0, 5, 0,  7, 2, 2, 2,  2, 12 ],
        [ 5,  0, 0, 0,  0, 5,  0, 5, 0, 5, 0,  5,  0, 5, 0,  5, 0, 0, 0,  0, 5  ],
        [ 5,  0, 7, 8,  0, 5,  0, 5, 0, 5, 0,  5,  0, 5, 0,  5, 0, 7, 8,  0, 5  ],
        [ 5,  0, 9, 10, 0, 5,  0, 5, 0, 9, 2,  10, 0, 5, 0,  5, 0, 7, 8,  0, 5  ],
        [ 5,  0, 0, 0,  0, 5,  0, 5, 0, 0, 0,  0,  0, 5, 0,  5, 0, 0, 0,  0, 5  ],
        [ 11, 2, 2, 3,  0, 6,  0, 6, 0, 1, 14, 3,  0, 6, 0,  6, 0, 1, 2,  2, 12 ],
        [ 5,  0, 0, 0,  0, 0,  0, 0, 0, 0, 5,  0,  0, 0, 0,  0, 0, 0, 0,  0, 5  ],
        [ 5,  0, 7, 8,  0, 4,  0, 1, 2, 2, 13, 2,  2, 3, 0,  4, 0, 7, 8,  0, 5  ],
        [ 5,  0, 9, 10, 0, 5,  0, 0, 0, 0, 0,  0,  0, 0, 0,  5, 0, 9, 10, 0, 5  ],
        [ 5,  0, 0, 0,  0, 5,  0, 0, 0, 0, 0,  0,  0, 0, 0,  5, 0, 0, 0,  0, 5  ],
        [ 9,  2, 2, 2,  2, 13, 2, 2, 2, 2, 2,  2,  2, 2, 2, 13, 2, 2, 2,  2, 10 ]
    ]

    property int resetX: 0
    property int resetY: 0

    property int boxWidth: scene.gameWindowAnchorItem.width
    property int boxHeight: scene.gameWindowAnchorItem.height
    property int numberOfBoxX: 21
    property int numberOfBoxY: 17
    property int sizeOfOneBoxX: boxWidth/numberOfBoxX

    property int movingPixels: 60
    property int checkNextX: 20
    property int checkNextY: 15
    // R, L, U, D
    property int movingDirection: 0

    property bool facingLeft: false
    property bool isPray: false

    Component.onCompleted: reset()

    GameSpriteSequence {
        id: walkingSequence
        anchors.centerIn: parent
        mirrorX: vacuum.facingLeft
        running: true

        GameSprite {
           id: walkingSequenceImage
           frameCount: 2
           frameRate: 10
           frameWidth: 50
           frameHeight: 23
           source: "../../../assets/vacuum.png"
        }
    }

    MovementAnimation {
        id: animationX
        target: parent
        property: "x"
        velocity: movingPixels
        running: false
        minPropertyValue: sizeOfOneBoxX/2
        maxPropertyValue: boxWidth - vacuum.width
        onLimitReached: {
            moveRandomly();
        }
    }

    MovementAnimation {
        id: animationY
        target: parent
        property: "y"
        velocity: movingPixels
        running: false
        minPropertyValue: sizeOfOneBoxX/2
        maxPropertyValue: boxHeight - vacuum.height
        onLimitReached: {
            moveRandomly();
        }
    }
    Timer {
        id: randomMoveTimer
        interval: 5000
        repeat: true
        onTriggered: {
            moveRandomly();
        }
    }

    Timer {
        id: checkPozTimer
        interval: 300
        repeat: true
        onTriggered: {
            if(!checkPosition(movingDirection)) {
                moveRandomly();
            }
        }
    }

    function reset() {
        randomMoveTimer.start();
        checkPozTimer.start();
        vacuum.x = resetX;
        vacuum.y = resetY;
    }

    function makePray() {
        isPray = true;
        movingPixels += 15;
        walkingSequenceImage.source = "../../../assets/prayVacuum.png";
    }

    function prayNoMore() {
        isPray = false;
        movingPixels -= 15;
        walkingSequenceImage.source = "../../../assets/vacuum.png";
    }

    function stopGame() {
        randomMoveTimer.stop();
        checkPozTimer.stop();
        walkingSequence.running = false;
        animationX.stop();
        animationY.stop();
    }

    function moveIfPossible(direction) {
        var nextPozI = 0;
        var nextPozJ = 0;
        switch(direction) {
            case 0:
                return goRight();
            case 1:
                return goLeft();
            case 2:
                return goUp();
            case 3:
                return goDown();
        }
        return true;
    }

    function moveRandomly() {
        let canMove = false;
        while(!canMove) {
            let direction = Math.floor(Math.random() * 4);
            canMove = moveIfPossible(direction);
        }

    }

    function changeDirectionToHorizontal() {
        animationY.stop();
        animationX.start();
        walkingSequence.running = true;
    }

    function changeDirectionToVertical() {
        animationX.stop();
        animationY.start();
        walkingSequence.running = true;
    }

    function goRight() {
        if(checkPosition(0)) {
            vacuum.state = "right";
            return true;
        }
        return false;
    }

    function goLeft() {
        if(checkPosition(1)) {
            vacuum.state = "left";
            return true;
        }
        return false;
    }

    function goUp() {
        if(checkPosition(2)) {
            vacuum.state = "up";
            return true;
        }
        return false;
    }

    function goDown() {
        if(checkPosition(3)) {
            vacuum.state = "down";
            return true;
        }
        return false;
    }

    function checkPosition(movingDir) {

        var nextPozI = 0;
        var nextPozJ = 0;

        // R, L, U, D
        switch(movingDir) {
            case 0:
                nextPozJ = Math.min(numberOfBoxX-1, Math.ceil((vacuum.x + vacuum.width/2 + checkNextX)/sizeOfOneBoxX)-1);
                nextPozI = Math.ceil((vacuum.y + vacuum.height/2)/sizeOfOneBoxX)-1;
                if(board[nextPozI][nextPozJ] !== 0) {
                    animationX.stop();
                    return false;
                }

                break;
            case 1:
                nextPozJ = Math.max(0, Math.ceil((vacuum.x + vacuum.width/2 - checkNextX)/sizeOfOneBoxX)-1);
                nextPozI = Math.ceil((vacuum.y + vacuum.height/2)/sizeOfOneBoxX)-1;
                if(board[nextPozI][nextPozJ] !== 0) {
                    animationX.stop();
                    return false;
                }
                break;
            case 2:
                nextPozJ = Math.ceil((vacuum.x + vacuum.width/2)/sizeOfOneBoxX)-1;
                nextPozI = Math.max(0, Math.ceil((vacuum.y + vacuum.height/2 - checkNextY)/sizeOfOneBoxX)-1);
                if(board[nextPozI][nextPozJ] !== 0) {
                    animationY.stop();
                    return false;
                }
                break;
            case 3:
                nextPozJ = Math.ceil((vacuum.x + vacuum.width/2)/sizeOfOneBoxX)-1;
                nextPozI = Math.min(numberOfBoxY-1, Math.ceil((vacuum.y + vacuum.height/2 + checkNextY)/sizeOfOneBoxX)-1);
                if(board[nextPozI][nextPozJ] !== 0) {
                    animationY.stop();
                    return false;
                }
                break;
        }
        return true;
    }

    state: "up"

    states: [
        State {
            name: "right"
            PropertyChanges {target: animationX; velocity: vacuum.movingPixels}
            StateChangeScript {
                script: {
                    movingDirection = 0;
                    changeDirectionToHorizontal();

                    facingLeft = false;
                }
            }
        },
        State {
            name: "left"
            PropertyChanges {target: animationX; velocity: -vacuum.movingPixels}
            StateChangeScript {
                script: {
                    movingDirection = 1;
                    changeDirectionToHorizontal();
                    facingLeft = true;
                }
            }
        },
        State {
            name: "up"
            PropertyChanges {target: animationY; velocity: -vacuum.movingPixels}
            StateChangeScript {
                script: {
                    movingDirection = 2;
                    changeDirectionToVertical();
                }
            }
        },
        State {
            name: "down"
            PropertyChanges {target: animationY; velocity: vacuum.movingPixels}
            StateChangeScript {
                script: {
                    movingDirection = 3;
                    changeDirectionToVertical();
                }
            }
        }
    ]

}
