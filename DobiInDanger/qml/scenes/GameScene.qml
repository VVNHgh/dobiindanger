import QtQuick
import Felgo
import "../entities"
import "../items"

SceneBase {
    id: scene

    property bool gameIsRunning: false
    property bool isPredatorDog: false
    property bool boneVisible: false
    property int kills: 0

    onKillsChanged: {
        if(kills >= 2) {
            dobi.gameWon();
        }
    }

    property var possibleBonePoz: [[scene.width/21, scene.height/17], [scene.width - scene.width/21*2, scene.height/17], [scene.width - scene.width/21*2, scene.height - scene.height/17*2]]

    Component.onCompleted: initGame()

    Dobberman {
        id: dobi
        resetX: scene.width/21 - 10
        resetY: scene.height/17 - 10
        boxWidth: scene.width
        boxHeight: scene.height
        onGameOver: {
            if(scene.state === "gameOver")
                return
            scene.state = "gameOver"
        }
        onGameWon: {
            if(scene.state === "win")
                return
            scene.state = "win"
        }

    }

    Vacuum {
        id: vacuum
        resetX: parent.width/2 - vacuum.width/2
        resetY: parent.height/2 - vacuum.height/2
        boxWidth: scene.width
        boxHeight: scene.height
    }

    Vacuum {
        id: vacuum2
        resetX: scene.width/21*6
        resetY: parent.height - vacuum2.height - 10
        boxWidth: scene.width
        boxHeight: scene.height
    }

    Bone {
        id: bone
        x: possibleBonePoz[0][0]
        y: possibleBonePoz[0][1]
        visible: boneVisible
    }

    Timer {
        id: checkCollideTimer
        interval: 300
        repeat: true
        onTriggered: {
            checkCollide();
        }
    }

    Timer {
        id: predatorTimer
        interval: 10000
        running: true
        onTriggered: {
            if(isPredatorDog) {
                dobiNotPredator();
            } else {
                if(!boneVisible) {
                    createBone();
                } else {
                    deleteBone();
                }
            }
        }
    }

    function checkCollide() {
        if(vacuum.opacity === 1) {
            if((((dobi.x < vacuum.x) && (dobi.x + dobi.width > vacuum.x)) || ((dobi.x > vacuum.x) && (dobi.x < vacuum.x + vacuum.width))) &&
                    (((dobi.y > vacuum.y) && (dobi.y < vacuum.y + vacuum.height)) || ((dobi.y < vacuum.y) && (dobi.y + dobi.height > vacuum.y)))) {
                if(isPredatorDog) {
                    vacuum.stopGame();
                    vacuum.opacity = 0;
                    kills += 1;
                } else {
                    dobi.gameOver();
                }
            }
        }

        if(vacuum2.opacity === 1) {
            if((((dobi.x < vacuum2.x) && (dobi.x + dobi.width > vacuum2.x)) || ((dobi.x > vacuum2.x) && (dobi.x < vacuum2.x + vacuum2.width))) &&
                    (((dobi.y > vacuum2.y) && (dobi.y < vacuum2.y + vacuum2.height)) || ((dobi.y < vacuum2.y) && (dobi.y + dobi.height > vacuum2.y)))) {
                if(isPredatorDog) {
                    vacuum2.stopGame();
                    vacuum2.opacity = 0;
                    kills += 1;
                } else {
                    dobi.gameOver();
                }
            }
        }

        if(boneVisible) {
            if((((dobi.x < bone.x) && (dobi.x + dobi.width > bone.x)) || ((dobi.x > bone.x) && (dobi.x < bone.x + bone.width))) &&
                    (((dobi.y > bone.y) && (dobi.y < bone.y + bone.height)) || ((dobi.y < bone.y) && (dobi.y + dobi.height > bone.y)))) {
                if(!isPredatorDog) {
                    makeDobiPredator();
                }
            }
        }
    }

    function createBone() {
        let poz = Math.floor(Math.random() * 3);
        bone.x = possibleBonePoz[poz][0];
        bone.y = possibleBonePoz[poz][1];
        boneVisible = true;
        predatorTimer.restart();
    }

    function deleteBone() {
        boneVisible = false;
        predatorTimer.restart();
    }

    function makeDobiPredator() {
        deleteBone();
        isPredatorDog = true;
        dobi.makePredator();
        vacuum.makePray();
        vacuum2.makePray();
        predatorTimer.restart();
    }

    function dobiNotPredator() {
        isPredatorDog = false;
        dobi.predatorNoMore();
        vacuum.prayNoMore()
        vacuum2.prayNoMore()
        predatorTimer.restart();
    }

    function initGame() {
        dobi.reset();
        vacuum.reset();
        vacuum2.reset();
        isPredatorDog = false;
        boneVisible = false;
        kills = 0;
        vacuum.opacity = 1;
        vacuum2.opacity = 1;
    }

    Keys.onDownPressed: {
        if(gameIsRunning) {
            dobi.goDown();
        }
    }

    Keys.onUpPressed: {
        if(gameIsRunning) {
            dobi.goUp();
        }
    }

    Keys.onRightPressed: {
        if(gameIsRunning) {
            dobi.goRight();
        }
    }

    Keys.onLeftPressed: {
        if(gameIsRunning) {
            dobi.goLeft();
        }
    }

    function startGame() {
        initGame();
        checkCollideTimer.start();
        predatorTimer.start();
    }

    function stopGame() {
        checkCollideTimer.stop();
        predatorTimer.stop();
        dobi.stopGame();
        vacuum.stopGame();
        vacuum2.stopGame();
    }

    function gameOver() {
        stopGame();
    }

    function gameWon() {
        stopGame();
    }

    function enterScene() {
        state = "play"
    }

    GameOverScene {
        id: gameOverStats
        onGamePressed: scene.state = "play"
    }

    Win {
        id: winScene
        onGamePressed: scene.state = "play"
    }

    state: "play"

    states: [
        State {
            name: "play"
            PropertyChanges {target: scene; gameIsRunning: true}
            PropertyChanges {target: gameOverStats; opacity: 0}
            PropertyChanges {target: winScene; opacity: 0}
            StateChangeScript {
                script: {
                    startGame();
                }
            }
        },
        State {
            name: "gameOver"
            PropertyChanges {target: gameOverStats; opacity: 1}
            PropertyChanges {target: winScene; opacity: 0}
            StateChangeScript {
                script: {
                    gameOver();
                }
            }
        },
        State {
            name: "win"
            PropertyChanges {target: gameOverStats; opacity: 0}
            PropertyChanges {target: winScene; opacity: 1}
            StateChangeScript {
                script: {
                    gameWon();
                }
            }
        }
    ]
}
