import QtQuick
import Felgo
import "../common"
import "../entities"
import "../items"

Item {
    id: winScene
    width: parent.width
    height: parent.height
    opacity: 0
    visible: opacity === 0 ? false : true
    enabled: visible

    signal gamePressed()

    MultiResolutionImage {
        anchors.top: parent.top
        anchors.topMargin: 60
        anchors.horizontalCenter: parent.horizontalCenter
        source: Qt.resolvedUrl("../../assets/win.png")
    }

    StartButton {
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottomMargin: 60
        onClicked: {
            gamePressed()
        }
    }
}
