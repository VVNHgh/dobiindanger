import QtQuick
import Felgo
import "../common"


Scene {
    id: sceneBase
    width: 420
    height: 340

    Background {
        anchors.horizontalCenter: sceneBase.gameWindowAnchorItem.horizontalCenter
        anchors.bottom: sceneBase.gameWindowAnchorItem.bottom
    }
}
