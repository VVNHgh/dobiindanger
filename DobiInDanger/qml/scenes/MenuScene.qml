import QtQuick
import Felgo
import "../entities"
import "../items"

SceneBase {
    id: scene

    signal gamePressed()

    MultiResolutionImage {
        anchors.top: parent.top
        anchors.topMargin: 60
        anchors.horizontalCenter: scene.gameWindowAnchorItem.horizontalCenter
        source: Qt.resolvedUrl("../../assets/title.png")
    }

    StartButton {
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: scene.gameWindowAnchorItem.horizontalCenter
        anchors.bottomMargin: 60
        onClicked: {
            gamePressed()
        }
    }

    Keys.onEnterPressed: {
        gamePressed()
    }
}
