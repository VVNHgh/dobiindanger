import QtQuick
import Felgo

Item {
    width: 420
    height: 340

    Rectangle { height: 1000; width: bg.width; y: parent.height; color: "#7E6B5D" }
    Rectangle { height: 1000; width: bg.width; y: -1000;         color: "#83D0F5" }

    MultiResolutionImage {
        id: bg
        source: Qt.resolvedUrl("../../assets/madeMaze.png")
    }
}
